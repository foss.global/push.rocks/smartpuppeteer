// node native scope
import * as os from 'os';

export { os };

// @pushrocks scope
import * as smartdelay from '@pushrocks/smartdelay';
import * as smartshell from '@pushrocks/smartshell';

export { smartdelay, smartshell };

// third party scope
import puppeteer from 'puppeteer';
import treeKill from 'tree-kill';

export { puppeteer, treeKill };
