import { getEnvAwareBrowserInstance } from './smartpuppeteer.classes.smartpuppeteer.js';
import * as plugins from './smartpuppeteer.plugins.js';

export class IncognitoBrowser {
  public status: 'started' | 'stopped' = 'stopped';
  public browser: plugins.puppeteer.Browser;

  constructor() {}

  /**
   * starts the IncognitoBrowser
   */
  public async start() {
    this.status = 'started';
    this.browser = await getEnvAwareBrowserInstance();
    this.browser.on('disconnected', async (eventArg) => {
      try {
        this.browser.removeAllListeners();
      } catch (err) {}
      if (this.status === 'started') {
        this.browser = await getEnvAwareBrowserInstance();
      }
    });
  }

  /**
   * stops the IncognitoBrowser
   */
  public async stop() {
    this.status = 'stopped';
    plugins.treeKill(this.browser.process()?.pid as number, 'SIGKILL');
    await this.browser.close();
  }

  /**
   * rotate
   */
  public async rotateBrowser() {
    this.browser.close().catch();
    this.browser = await getEnvAwareBrowserInstance();
  }

  public async getNewIncognitoContext(): Promise<plugins.puppeteer.BrowserContext> {
    if (this.browser) {
      return this.browser.createIncognitoBrowserContext();
    } else {
      throw new Error('you need to start the IncognitoBrowser instance first');
    }
  }
}
