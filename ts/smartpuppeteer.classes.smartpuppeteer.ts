import * as plugins from './smartpuppeteer.plugins.js';

export interface IEnvAwareOptions {
  forceNoSandbox?: boolean;
  usePipe?: boolean;
}

export const getEnvAwareBrowserInstance = async (
  optionsArg: IEnvAwareOptions = {}
): Promise<plugins.puppeteer.Browser> => {
  const options: IEnvAwareOptions = {
    ...{
      forceNoSandbox: false,
    },
    ...optionsArg,
  };

  let chromeArgs: string[] = [];
  if (process.env.CI || options.forceNoSandbox || plugins.os.userInfo().username === 'root') {
    chromeArgs = chromeArgs.concat(['--no-sandbox', '--disable-setuid-sandbox']);
  }

  let headlessBrowser: plugins.puppeteer.Browser;
  console.log('launching puppeteer bundled chrome with arguments:');
  console.log(chromeArgs);
  headlessBrowser = await plugins.puppeteer.launch({
    args: chromeArgs,
    pipe: options.usePipe !== undefined ? options.usePipe : true,
    headless: true,
    ...(() => {
      const returnObject: any = {};
      const googleChrome = plugins.smartshell.which.sync('google-chrome');
      if (googleChrome) {
        returnObject.executablePath = googleChrome;
      }
      return returnObject;
    })(),
  });

  return headlessBrowser;
};
