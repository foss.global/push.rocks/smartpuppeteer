/**
 * autocreated commitinfo by @pushrocks/commitinfo
 */
export const commitinfo = {
  name: '@pushrocks/smartpuppeteer',
  version: '2.0.2',
  description: 'simplified access to puppeteer'
}
