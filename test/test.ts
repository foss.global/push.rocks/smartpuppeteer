import { expect, tap } from '@pushrocks/tapbundle';
import * as smartpuppeteer from '../ts/index.js';

tap.test('should use pipe', async (tools) => {
  const headlessBrowser = await smartpuppeteer.getEnvAwareBrowserInstance({
    forceNoSandbox: false,
  });
  const page = await headlessBrowser.newPage();
  await page.goto('https://lossless.com');
  console.log(await page.title());
  expect(headlessBrowser.wsEndpoint()).toEqual('');
  await headlessBrowser.close();
});

tap.test('should use websocket', async (tools) => {
  const headlessBrowser = await smartpuppeteer.getEnvAwareBrowserInstance({
    forceNoSandbox: false,
    usePipe: false,
  });
  const page = await headlessBrowser.newPage();
  await page.goto('https://lossless.com');
  console.log(await page.title());
  expect(headlessBrowser.wsEndpoint()).not.toEqual('');
  await headlessBrowser.close();
});

tap.test('should get and stop an Incognito browser', async () => {
  const incognitoInstance = new smartpuppeteer.IncognitoBrowser();
  await incognitoInstance.start();
  const page = await incognitoInstance.browser.newPage();
  await incognitoInstance.stop();
});

tap.start();
